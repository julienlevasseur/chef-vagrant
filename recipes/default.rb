#
# Cookbook:: vagrant
# Recipe:: default
#

if node['platform_family'] == 'debian'
  remote_file "#{node['vagrant']['install_dir']}/vagrant_#{node['vagrant']['version']}_x86_64.deb" do
    source "https://releases.hashicorp.com/vagrant/#{node['vagrant']['version']}/vagrant_#{node['vagrant']['version']}_x86_64.deb"
    mode '0644'
    not_if { ::File.exist?("#{node['vagrant']['install_dir']}/vagrant_#{node['vagrant']['version']}_x86_64.deb") }
  end

  dpkg_package "vagrant_#{node['vagrant']['version']}_x86_64.deb" do
    action :install
    source "#{node['vagrant']['install_dir']}/vagrant_#{node['vagrant']['version']}_x86_64.deb"
  end

  node.default['vagrant']['install_dir'] = '/usr/bin'
else
  directory node['vagrant']['install_dir'] do
    recursive true
  end

  directory node['vagrant']['config_dir'] do
    recursive true
  end

  directory node['vagrant']['data_dir'] do
    recursive true
  end

  remote_file "#{node['vagrant']['install_dir']}/vagrant.zip" do
    source "https://releases.hashicorp.com/vagrant/#{node['vagrant']['version']}/vagrant_#{node['vagrant']['version']}_linux_amd64.zip"
    mode '0755'
    not_if { ::File.exist?("#{node['vagrant']['install_dir']}/vagrant.zip") }
    not_if { ::File.exist?("#{node['vagrant']['install_dir']}/vagrant") }
  end

  zipfile "#{node['vagrant']['install_dir']}/vagrant.zip" do
    into "#{node['vagrant']['install_dir']}/"
    only_if { ::File.exist?("#{node['vagrant']['install_dir']}/vagrant.zip") }
  end

  file "#{node['vagrant']['install_dir']}/vagrant.zip" do
    action :delete
  end
end

execute 'install_vagrant-winrm_plugin' do
  command 'vagrant plugin install vagrant-winrm'
  action :run
  not_if 'vagrant plugin list|grep vagrant-winrm'
end

if Chef::Config[:file_cache_path].include?('kitchen')
  node.save # ~FC075
  file '/tmp/kitchen/nodes/node.json' do
    owner 'root'
    group 'root'
    mode 0755
    content ::File.open("/tmp/kitchen/nodes/#{node.name}.json").read
    action :create
    only_if { ::File.exist?("/tmp/kitchen/nodes/#{node.name}.json") }
  end
end

name 'vagrant'
maintainer 'Julien Levasseur'
maintainer_email 'contact@julienlevasseur.ca'
license 'MIT'
description 'Installs/Configures vagrant'
long_description 'Installs/Configures vagrant'
version '0.1.0'
chef_version '>= 12.14' if respond_to?(:chef_version)
issues_url 'https://gitlab.com/julienlevasseur/chef-vagrant/issues'
source_url 'https://gitlab.com/julienlevasseur/chef-vagrant'

depends 'zipfile'

supports 'ubuntu'
supports 'centos'
supports 'debian'

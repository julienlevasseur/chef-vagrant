
default['vagrant']['version'] = '2.1.2'

default['vagrant']['install_dir'] = '/usr/local/bin'
default['vagrant']['config_dir']  = '/etc/vagrant'
default['vagrant']['data_dir']    = '/var/lib/vagrant'

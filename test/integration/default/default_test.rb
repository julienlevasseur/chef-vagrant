# # encoding: utf-8

# Inspec test for recipe terraform::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

require 'json'
nodefile = '/tmp/kitchen/nodes/node.json'
node = json(nodefile).params if File.exist?(nodefile) || json('/tmp/kitchen/nodes/node.json').params

describe file("#{node['default']['vagrant']['install_dir']}/vagrant") do
  it { should exist }
end

describe command("#{node['default']['vagrant']['install_dir']}/vagrant -v|head -1|awk '{print $2}'") do
  its('stdout') { should match node['default']['vagrant']['version'] }
end

describe command("#{node['default']['vagrant']['install_dir']}/vagrant plugin list") do
  its('stdout') { should match 'vagrant-winrm' }
end
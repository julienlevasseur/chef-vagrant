name 'default'
default_source :supermarket
default_source :chef_repo, '..'
cookbook 'vagrant', path: '../../..'
run_list 'vagrant::default'
